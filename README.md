##Site (en)
##Description
Create a responsive site that shows a listing of the seasons of a show or show of your choice upon clicking. Follow the mockup below to do something similar to make the mobile version of the site.
Mockups

https://ibb.co/mwa52G

https://ibb.co/bR8Q2G

In order to create this app you will use the Trakt API (you must sign up in the website to have access to the API).

##Requirements


It’s allowed to use external libraries

Handle error scenarios (server errors)

Loading feedback

Automated tests

Cache of images and API


###Submission
The candidate must implement the solution and send a pull request to this repository with the solution.

The Pull Request process works as follows:

1.Candidate will fork this repository (will not clone directly!)

2.It will make your project on that fork.

3.Commit and upload changes to your fork.

4.Through the Bitbucket interface, you will send a Pull Request.

5.If possible to leave the public fork to facilitate inspection of the code
